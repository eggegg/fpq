#ifndef __CONFIG_H__
#define __CONFIG_H__

#define DEBUG

#define FIRST
#undef SECOND
#define THIRD
#define LAST

#define TRIM

#define ASSIGN_RESTART_LIMIT -1
#define ASSIGN_TIME_LIMIT 0
#define LAST_RESTART_LIMIT 500
#define LAST_TIME_LIMIT 0

#endif /* __CONFIG_H__ */

