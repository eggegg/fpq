#ifndef __PROCEDURE_H__
#define __PROCEDURE_H__

void firstPass();
void secondPass() ;
void thirdPass();
void lastPass();
void printOutput(char*);
void printStat();

#endif /* __PROCEDURE_H__ */

