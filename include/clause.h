#ifndef __CLAUSE_H__
#define __CLAUSE_H__

#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <list>
#include "sat.h"
#include "parser.h"
using namespace std;

class Assignment {

friend class SatAgent;

friend ostream& operator<<(ostream&, const Assignment&);
// output assignment in MINISAT format

public:
   Assignment() {}
   bool getValue(int) const;
	void setValue(int,bool);
   void show(FILE * out = NULL) const ;
private:
   std::map<int, bool> _map;
};

class Clauses {

friend class SatAgent;
friend class TrimAgent;
//friend Clauses& operator+=(const Clauses &b);
//friend const Clauses operator+(const Clauses &b) const;

friend ostream& operator<<(ostream&, const Clauses&);
// output Clauses in Dimac format, Clauses sign are assumed to have 1 for true, 0 for false

public:
   Clauses();
   bool isValid(int);  // check if the variable is in a clause
	Clauses& operator+=(const Clauses &b);
	const Clauses operator+(const Clauses &b) const; // merge two set of clauses
	bool verify(const Assignment &a) const; // see if assignment a satisfy the clauses

protected:
   vector<vector<int> > clauses;
   vector<vector<bool> > sign;
   set<int> variables;
};

enum MutantType {
   POSITIVE,
   NEGATIVE,
   TOGGLE
};

enum MutantStatus {
   ALIVE,
   DEAD,
   UNKNOWN
};

class Model;
class Property;

class Mutant {
friend class SatAgent;
public:
   /* dummy constructor, used by new [] */
   Mutant() {}
   Mutant(MutantType, int , string);

   const char* getName() const { return name.c_str(); }
   int getVariable() const { return literal; }
   MutantType getType() const {return type ;}
   MutantStatus getStatus() const { return alive; }
   void setAlive() { alive = ALIVE; }
   void setDead() { alive = DEAD; }
   // saving the index of properties
   list<Property*> property_list;

private:
   string name;
   MutantType type;
   MutantStatus alive;
   int literal;
};

enum PropertyStatus {
	P_UNKNOWN,
	P_SAT,
	P_UNSAT,
	P_GIVENUP
};

class Property : public Clauses {
friend class SatAgent;
friend void parseSingleProperty(const char* , const char* , Property*);
friend void generateAssignment(Property*);
public:
   const char* getName() const { return name.c_str(); }
   Assignment getAssignment() const { return assignment; }
   /*
    * getSign:
    *    if all var in property is positive, return > 0
    *    if all var in property is negative, return < 0
    *    else                                return   0
    */
   int getSign(int var) const ;
   Property() : status(P_UNKNOWN) {}

	PropertyStatus status ;  // UNKNOWN / SAT / UNSAT / GIVENUP
private:
   Assignment assignment;
   string name;
};

class Model : public Clauses {
friend class SatAgent;
friend void generateModel(const char*);
public:
   Model() {}
   Model getModel(Mutant&);
};

#endif /* __CLAUSE_H__ */

