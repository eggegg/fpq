#ifndef __SAT_H__
#define __SAT_H__

#include <map>

#define MINISAT

#ifdef MINISAT
#include "minisat/core/Solver.h"
#define VARIABLE Var
#define LITERAL  Lit
#define SOLVER   Solver
#define VECTOR   vec
#define PUSH     push
using namespace Minisat;
#endif

// Dummy class for compiling issues
enum SatReason {
   UNDEF = -1,
   SAT = 0,
   UNSAT = 1,
   GIVEUP = 2
};

class Clauses;
class Property;
class Model;
class Mutant;
class Assignment;

class SatClause {
public:
   SatClause() {}

   void addLiteral(VARIABLE&, bool);
   VECTOR<LITERAL>& getClause() { return _clause; }
private:
   VECTOR<LITERAL> _clause;
};

class SatAgent {
public:
    SatAgent();
    ~SatAgent();

    VARIABLE newVariable();
    void assumeVariable(VARIABLE&, bool);
    void addClauses(Clauses&);

    Assignment getAssignment();
    SatReason getReason();
    bool solve();
    bool solve(LITERAL) ;

    inline void checkGarbage() {_solver->checkGarbage() ;}

    inline void setRestartLimit(int x) {_solver->restart_limit = x ;}
    inline void setTimeLimit(size_t x) {_solver->time_limit = x ;}

private:
    bool getValue(int);
    void addClause(SatClause&);

    std::map<int, VARIABLE> _map;
    SOLVER* _solver;
    VECTOR<LITERAL> _assumptions;
};

#endif /* __SAT_H__ */

