#ifndef _PARSER_H_
#define _PARSER_H_

extern int parseMutant(const char*) ;
extern int parseProperty(const char*) ;
extern void generateModel(const char*) ;
extern void generateAssignment() ;

#endif // _PARSER_H_

