/*
 * This file defines some macros for debug purpose.  To use these macros, add
 *    -DDEBUG
 * when compiling. If DEBUG is not defined, these macros are still defined but
 * doing nothing at all.
 */
#ifndef _DEBUG_H_
#define _DUBUG_H_

#include "config.h"

#ifdef DEBUG
#  include<iostream>
/*
 * SHOWVAR(x)
 *    this macro prints out the value of the variable x with its name, e.g.
 *    ------- CODE -------
 *    void fun() {
 *       int abc = 123 ;
 *       SHOWVAR(abc) ;
 *    }
 *    ------ OUTPUT ------
 *    [fun] abc = 123
 *    --------------------
 */
#  define SHOWVAR(x) \
   (std::cout << "[" << __func__ << "] " << (#x) << " = " << (x) << std::endl)
/*
 * dprintf
 *    this macro acts like printf, but print out function name before the
 *    message
 */
#  define dprintf(msg , args...) printf("[%s] " msg , __func__ , ##args)
/*
 * cdebug
 *    just like cout. It's no supported currently, since it's difficult to
 *    handle when DEBUG is not defined.
 * #  define cdebug     (std::cout << "[" << __func__ << "] ")
 */
#else
/*
 * if debug is not defined, macros do nothing
 */
#  define SHOWVAR(x)
#  define dprintf(args...)
#endif // DEBUG

#endif // _DEBUG_H_
