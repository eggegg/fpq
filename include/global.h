#ifndef __GLOBAL_H__
#define __GLOBAL_H__
#include <list>
#include <time.h>

extern int mutant_num, property_num;
extern Model model;
extern Mutant *mutants;
extern Property *properties;
extern std::list<Mutant*> mutant_list;
extern time_t start_time , pre_time;

#endif /* __GLOBAL_H__ */

