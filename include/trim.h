/****************************************************************************
	USAGE:
	trim down inpput clauses by repetitively eliminating constant variables
	and correlated ones, then return the trimmed clauses and variable map.
   NOTE:
	since constant variables could be trimmed down then discarded, use this
	function on FINAL CNFs only, i.e. do NOT trimmed then add new clauses.
****************************************************************************/

#ifndef __TRIM_H__
#define __TRIM_H__

#include <utility>
#include <map>
#include "clause.h"

using namespace std;

const int _trim_false = 1000000000;
const int _trim_true = 1000000001;

class DNode;

/* given assignment [orig] to reduced set and rmap of relations between *
 * reduced set and original set, return the assignment for original set */
Assignment trim_remap(const Assignment &reduced, map<int,int>rmap);

/* stores two-variable relation */
class BiClause {
	public:
		int v,u;
		BiClause();
		BiClause(int vi,int ui);
		BiClause rev() const;
		bool relsign() const;
		const bool operator<(const BiClause &b) const;
};


class TrimAgent {

public:
   pair<Clauses, map<int,int> > trim(Clauses inputClauses, bool &satFlag);
   // parse clauses and return the trimmed version + corresponding variable map
   // if clauses conflicting (proven to be UNSAT), satFlag set to 0, 1 otherwise
   // in the case of UNSAT, returned clauses is not guaranteed to take any form
   /****************************************************************************
   	NOTE for variable map:
      if map[v] = _trim_false  if v must be false
                = _trim_true   if v must be true
                = u/-u         if v equals to varaible u/!u
   ****************************************************************************/
   pair<Clauses, map<int,int> > trim(Clauses inputClauses);
   // no-satFlag version, only use when clauses known to be SAT


protected:
	// class members
   int vmax;
   map<int,int> rmap;
   DNode *vset;
   bool failflag;

	// helper functions
	void initialize(const Clauses&);
	void finalize();
	bool elimConstVar(Clauses&);
	bool elimCorrelated(Clauses&);
	void updateVariablesSet(Clauses&);
	int getSymbol(int);

};

#endif
