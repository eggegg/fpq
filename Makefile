TARGET=fpq

# all object files are placed in this directory
OBJDIR=obj

# all source code (*.cpp, *.c) are placed in this directory
SRCDIR=src

# all header files (*.hpp, *.h) are placed in this directory
HDRDIR=include

# minisat include
MINISAT_CXXFLAGS = -Iminisat -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS
MINISAT_LIB = -Lminisat/build/release/lib -lminisat

LIB=minisat/build/release/lib/libminisat.a
OBJECT=$(OBJDIR)/main.o $(OBJDIR)/parser.o $(OBJDIR)/sat.o $(OBJDIR)/clause.o $(OBJDIR)/procedure.o $(OBJDIR)/trim.o

CXX   = g++
LD    = g++
#C_FLAGS= -g -I$(HDRDIR) $(MINISAT_CXXFLAGS)
C_FLAGS= -O3 -I$(HDRDIR) $(MINISAT_CXXFLAGS)
LIB_FLAGS= $(MINISAT_LIB)

ALL : $(TARGET)

minisat/build/release/lib/libminisat.a:
	cd minisat; make;

$(TARGET) : $(OBJECT) $(LIB)
	$(LD) $(LD_FLAGS) -o $@ $^ $(LIB_FLAGS)

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(C_FLAGS) -o $@ -c $<

.PHONY: clean
clean :
	-rm -f $(OBJECT) *.o

cleanall : clean
	cd minisat; make clean;

