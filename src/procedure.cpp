#include <stdio.h>
#include "procedure.h"
#include "clause.h"
#include "global.h"
#include "parser.h"
#include "debug.h"
#include "trim.h"
#include "config.h"

void firstPass() {
   for(list<Mutant*>::iterator mut = mutant_list.begin();
         mut != mutant_list.end(); /* ++mut */ ) {
      int before = (int)(*mut)->property_list.size() ;

      for(list<Property*>::iterator prop = (*mut)->property_list.begin();
            prop != (*mut)->property_list.end();/* ++prop */) {
         bool erased = false ;
         if (!(*prop)->isValid((*mut)->getVariable())) {
            dprintf("remove %s from %s\n" , (*prop)->getName() , (*mut)->getName()) ;
            erased = true ;
         }

         if (erased) {
            prop = (*mut)->property_list.erase(prop);
         } else {
            ++ prop ;
         }
      }

      if((*mut)->property_list.size() == 0) {
         dprintf("%s is set alive in %s\n" , (*mut)->getName() , __func__) ;
         (*mut)->setAlive();
         mut = mutant_list.erase(mut);
      } else {
         ++ mut ;
      }
   }
}

void secondPass() {
   for(list<Mutant*>::iterator mut = mutant_list.begin();
         mut != mutant_list.end(); /* ++mut */ ) {
      int before = (int)(*mut)->property_list.size() ;

      for(list<Property*>::iterator prop = (*mut)->property_list.begin();
            prop != (*mut)->property_list.end();/* ++prop */) {
         bool erased = false ;
         if ((*mut)->getType() != TOGGLE) {
            int sign = (*prop)->getSign((*mut)->getVariable()) ;
            if ((*mut)->getType() == POSITIVE && sign > 0) {
               erased = true ;
            } else if ((*mut)->getType() == NEGATIVE && sign < 0) {
               erased = true ;
            }
         }

         if (erased) {
            prop = (*mut)->property_list.erase(prop);
         } else {
            ++ prop ;
         }
      }

      if((*mut)->property_list.size() == 0) {
         dprintf("%s is set alive in %s\n" , (*mut)->getName() , __func__) ;
         (*mut)->setAlive();
         mut = mutant_list.erase(mut);
      } else {
         ++ mut ;
      }
   }
}

void thirdPass() {
   dprintf("Start third pass\n");
   for(list<Mutant*>::iterator mut = mutant_list.begin();
       mut != mutant_list.end();) {

		if((*mut)->getType() == TOGGLE) {
			dprintf("Give up %s since it's TOGGLE\n", (*mut)->getName());
                        ++mut;
			continue;
		}
		// no hope in deleting this mutant using third pass

      for(list<Property*>::iterator prop = (*mut)->property_list.begin();
          prop != (*mut)->property_list.end();) {

         bool vsign ;
         if ((*prop)->status == P_UNKNOWN) {
            generateAssignment((*prop));
         }

			if((*prop)->status == P_GIVENUP) {
				++prop;
				continue;
			}

         vsign = (*prop)->getAssignment().getValue((*mut)->getVariable()) ;
			
         if(((*mut)->getType() == POSITIVE &&  vsign) ||
            ((*mut)->getType() == NEGATIVE && !vsign)) {
            dprintf("remove %s from %s\n" , (*prop)->getName() , (*mut)->getName()) ;
				/*debug*/ {
					Assignment sol = (*prop)->getAssignment();
					sol.setValue((*mut)->getVariable(),(*mut)->getType()==POSITIVE?1:0);
					// sol is the solution to current mut-prop
					Model mcls;
					Clauses cls;
					mcls = model.getModel(**mut);
					cls = mcls + **prop;
					// cls is the clauses that represents current mutant-property pair
					if(!cls.verify(sol)) {
						// verification fail: obtained solution is somehow wrong
						dprintf("verification failed, assignment is wrong\n");
					}
				}
            prop = (*mut)->property_list.erase(prop);
         } else {
            prop++;
         }
      }
      if((*mut)->property_list.size() == 0) {
         dprintf("%s is set alive in %s\n" , (*mut)->getName() , __func__) ;
         (*mut)->setAlive();
         mut = mutant_list.erase(mut);
      } else {
         ++ mut ;
      }
   }
}

void lastPass() {
   int i = 0 , size = mutant_list.size() ;
   for(list<Mutant*>::iterator mut = mutant_list.begin();
       mut != mutant_list.end(); ++mut) {
      bool alive = true;
      dprintf("lastPass for %s (%d/%d)\n" , (*mut)->getName() , ++ i , size) ;
      for(list<Property*>::iterator prop = (*mut)->property_list.begin();
          prop != (*mut)->property_list.end(); ++prop) {
         SatAgent sat;
         Clauses cnf = model.getModel(**mut) + **(prop) ;
#ifdef TRIM
         TrimAgent ta;
         bool satflag;

         cnf = ta.trim(cnf, satflag).first;
	 if(!satflag) {
	    dprintf("%s is killed by %s in TRIM\n", (*mut)->getName(), (*prop)->getName());
	    alive = false;
	    break;
	 }
#endif
         sat.addClauses(cnf);

         sat.setRestartLimit(LAST_RESTART_LIMIT);
         sat.setTimeLimit(LAST_TIME_LIMIT);

         if(!sat.solve()) {
            dprintf("%s is killed by %s\n" , (*mut)->getName() , (*prop)->getName()) ;
            alive = false;
            break;
         }
         // NOTE: remove prop if not last pass
      }
      if(alive) {
         dprintf("%s is set alive in %s\n" , (*mut)->getName() , __func__) ;
         (*mut)->setAlive();
      } else {
         (*mut)->setDead();
      }
      // NOTE: remove mut if not last pass
   }
}

void printStat() {
   int remain_prop = 0, remain_mut = 0 ;
   time_t cur_time = time(NULL) ;

   for(int i = 0; i < mutant_num; ++i)
      if(mutants[i].getStatus() == UNKNOWN) {
         remain_prop += mutants[i].property_list.size();
         remain_mut++;
      }
   dprintf("Time: %d (%d) \tMutants left: %d \tProperties left: %d \n",
           (int)(cur_time - start_time), (int)(cur_time - pre_time) , remain_mut, remain_prop);
   pre_time = cur_time ;
}

void printOutput(char* file) {
   FILE* fp = fopen(file, "w");
   for(int i = 0; i < mutant_num; ++i)
      if(mutants[i].getStatus() == ALIVE)
         fprintf(fp, "%s\n", mutants[i].getName());
   fclose(fp);
}

