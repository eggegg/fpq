#include <iostream>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "clause.h"
#include "parser.h"
#include "debug.h"
#include "global.h"
#include "procedure.h"
#include "trim.h"
#include "config.h"

#define __TRIMTEST

void parseSingleMutant     (const char * , const char * , Mutant *) ;
void parseSingleProperty   (const char * , const char * , Property *) ;

static char * getFileName(const char *,const char *) ;

int parseMutant(const char * file) {
   char buf[1024] = {0} , *fname , *fpath ;
   FILE * mutantList = fopen(file , "r") ;
   int remainSize , i;

   assert(mutantList != NULL) ;

   /*
    * The first line is # of mutant files
    */
   fgets(buf , 1024 , mutantList) ;
   mutant_num = atoi(buf) ; /* defined in global.h */

   if (mutants != NULL) {
      delete [] mutants ;
   }

   mutants = new Mutant[mutant_num] ;

   assert(mutantList != NULL) ;

   strcpy(buf , file) ;
   fname = getFileName(buf , buf + 1023) ;
   remainSize = buf + 1024 - fname ;

   fpath = buf ;
   for (i = 0 ; fgets(fname , remainSize , mutantList) ; ++ i) {
      fname[strlen(fname) - 1] = 0 ; // the last char is '\n'
      // now fpath is the path to a mutant file
      parseSingleMutant(fpath , fname , mutants + i) ;
      mutant_list.push_back(mutants + i) ;
   }

   if (i != mutant_num) {
      // there must be something wrong
      // TODO: raise exception
   }

   fclose(mutantList) ;
}

int parseProperty(const char * file) {
   char buf[1024] = {0} , *fname , *fpath ;
   FILE * propList = fopen(file , "r") ;
   int remainSize , i;

   assert(propList != NULL) ;

   /*
    * The first line is # of mutant files
    */
   fgets(buf , 1024 , propList) ;
   property_num = atoi(buf) ; /* defined in global.h */

   if (properties != NULL) {
      delete [] properties ;
   }

   properties = new Property[property_num] ;

   assert(properties != NULL) ;

   strcpy(buf , file) ;
   fname = getFileName(buf , buf + 1023) ;
   remainSize = buf + 1024 - fname ;

   fpath = buf ;
   for (i = 0 ; fgets(fname , remainSize , propList) ; ++ i) {
      fname[strlen(fname) - 1] = 0 ; // the last char is '\n'
      // now fpath is the path to a property file
      parseSingleProperty(fpath , fname , properties + i) ;
   }

   if (i != property_num) {
      // there must be something wrong
      // TODO: raise exception
   }

   fclose(propList) ;
}

void generateModel(const char * dir) {
   int second_mut = -1;
   char buf[1024] , *fname , *fpath;

   /*
    * First, generate fpath
    */
   strcpy(buf , dir) ;
   fpath = buf ;
   fname = getFileName(buf , buf + 1023) ;

   if (mutants[0].getType() == TOGGLE) {
      // This single file is enough to generate the correct model
      FILE * mfile ;
      int maxvar , nclauses , thisvar;
      vector<int>    tmp_vars ;
      vector<bool>   tmp_sign ;

      thisvar = mutants[0].getVariable() ;

      strcpy(fname , mutants[0].getName()) ;
      mfile = fopen(fpath , "r") ;
      assert(mfile != NULL) ;

      // looking for the first not comment line
      for (;fgets(buf , 1024 , mfile) && buf[0] == 'c';) ;

      // this line should be
      //    p cnf maxvar nclauses
      sscanf(buf , "p cnf %d %d" , &maxvar , &nclauses) ;

      for (int i = 0 ; i < nclauses ; ++ i) {
         tmp_vars.clear() ;
         tmp_sign.clear() ;
         for (int var ; fscanf(mfile , "%d" , &var) && var != 0 ;) {
            bool sign = (var > 0) ;
            var = (var > 0 ? var : -var) ;
            if (var == thisvar) {
               sign = !sign ;
            }
            tmp_vars.push_back(var) ;
            tmp_sign.push_back(sign) ;
            model.variables.insert(var) ;
         }
         model.clauses.push_back(tmp_vars) ;
         model.sign.push_back(tmp_sign) ;
      }

      fclose(mfile) ; // done
      /*
       * for debug
       */
      goto FUNC_END ;
      return ;
   }

   for(int i = 1; i < mutant_num; ++i) {
      if(mutants[i].getVariable() != mutants[0].getVariable()) {
         second_mut = i;
         break;
      }
   }

   if(second_mut == -1) {
      // cannot generate original model
      // TODO: raise exception
   } else {
      /*
       * In this case, we assume that two files are same except for mutant var
       */
      int mvar1 , mvar2 , maxvar , nclauses;
      FILE * mfile1 ;
      FILE * mfile2 ;
      vector<int>    tmp_vars ;
      vector<bool>   tmp_sign ;

      mvar1 = mutants[0].getVariable() ;
      mvar2 = mutants[second_mut].getVariable() ;

      strcpy(fname , mutants[0].getName()) ;
      mfile1 = fopen(fpath , "r") ; // open first file
      assert(mfile1 != NULL) ;

      strcpy(fname , mutants[second_mut].getName()) ;
      mfile2 = fopen(fpath , "r") ; // open second file
      assert(mfile2 != NULL) ;

      // looking for the first not comment line
      for (;fgets(buf , 1024 , mfile1) && buf[0] == 'c';) ;
      for (;fgets(buf , 1024 , mfile2) && buf[0] == 'c';) ;

      // this line should be
      //    p cnf maxvar nclauses
      sscanf(buf , "p cnf %d %d" , &maxvar , &nclauses) ;

      for (int i = 0 ; i < nclauses ; ++ i) {
         tmp_vars.clear() ;
         tmp_sign.clear() ;
         for (int var1 , var2 ; ~fscanf(mfile1 , "%d" , &var1) &&
               ~fscanf(mfile2 , "%d" , &var2) && var1 != 0 && var2 != 0;) {
            /*
             * abs(var1) == abs(var2) should always be true
             */
            int  var = var1 > 0 ? var1 : -var1 ;
            bool sign ;

            if       (var1 == var2) sign = var1 > 0 ; // both are correct
            else if  (var == mvar1) sign = var2 > 0 ; // var2 is correct
            else                    sign = var1 > 0 ; // var1 is correct

            tmp_vars.push_back(var) ;
            tmp_sign.push_back(sign) ;
            model.variables.insert(var) ;
         }
         model.clauses.push_back(tmp_vars) ;
         model.sign.push_back(tmp_sign) ;
      }

      fclose(mfile1) ;
      fclose(mfile2) ;
   }
/*
 * for debug, print out model
 */
FUNC_END:
   if (0) {
      SHOWVAR(model.clauses.size()) ;
      for (int csize = model.clauses.size() , i = 0 ; i < csize ; ++ i) {
         for (int size = model.clauses[i].size() , j = 0 ; j < size ; ++ j) {
            printf("%d " , (model.sign[i][j] ? model.clauses[i][j] : -model.clauses[i][j])) ;
         }
         printf("\n") ;
      }
      printf("\n") ;
      getchar() ;
   }
   return ;
}

void generateAssignment(Property* prop) {
   int nsat = 0 ;

   printStat() ;
   SatAgent sat ;

   dprintf("Now solving property %s\n" , prop->getName()) ;

	Clauses cnf = model + *(prop) ;

#ifdef TRIM
	TrimAgent ta ;
	bool satflag;
	pair<Clauses, map<int,int> > trim_sol;
	Clauses origcnf = cnf;
	map<int,int> trim_map;

	trim_sol = ta.trim(cnf,satflag);
	cnf = trim_sol.first;
	trim_map = trim_sol.second;
	if(!satflag) {
		// propery UNSAT declared by TRIM
		dprintf("Assignment UNSAT by TRIM!!\n");
                prop->status = P_UNSAT;
	}
#endif

	sat.addClauses(cnf) ;

   sat.setRestartLimit(ASSIGN_RESTART_LIMIT) ;
   sat.setTimeLimit(ASSIGN_TIME_LIMIT) ;

   if (!sat.solve()) {

		if(sat.getReason()==UNSAT) {
                        dprintf("Assignment UNSAT by SAT!!\n");
			prop->status = P_UNSAT;
		} else {
			dprintf("Timeout, giveup %s...\n" , prop->getName()) ;
			prop->status = P_GIVENUP;
		}
		
   } else {
      dprintf("Assignment found!!\n");
#ifdef TRIM
		Assignment reduced_asn = sat.getAssignment();
		Assignment original_asn = trim_remap(reduced_asn,trim_map);
      prop->assignment = original_asn;

		/*DEBUG*/ {
			if(!origcnf.verify(prop->assignment)) {
				dprintf("faulty assignment generated (with TRIM)\n");
			}
		}

#else
		prop->assignment = sat.getAssignment();
#endif

      ++ nsat ;
      prop-> status = P_SAT ;
   }

   printStat() ;

   dprintf("sat/total: %d/%d\n" , nsat , property_num) ;
}

// helper function for parsing a single mutant file
void parseSingleProperty(const char * file , const char * fname , Property * prop) {
   FILE* fprop = NULL ;
   int maxvar , nclauses ;

   prop->name = fname ;
   fprop = fopen(file , "r") ;
   if (fprop == NULL) {
      // TODO: raise error
   }
   /*
    * The first line:
    * p cnf %d %d
    */
   fscanf(fprop , "p cnf %d %d" , &maxvar , &nclauses) ;

   vector<int> tmp_var ;
   vector<bool> tmp_sign ;
   for (int i = 0 ; i < nclauses ; ++ i) {
      tmp_var.clear() ;
      tmp_sign.clear() ;
      for (int var ; fscanf(fprop , "%d" , &var) , (var != 0) ;) {
         tmp_sign.push_back(var > 0) ;
         var = (var > 0 ? var : - var) ;
         tmp_var.push_back(var) ;
         prop->variables.insert(var) ;
      }
      prop->clauses.push_back(tmp_var) ;
      prop->sign.push_back(tmp_sign) ;
   }
   fclose(fprop) ;
}

// helper function for parsing a single mutant file
void parseSingleMutant(const char * file , const char * fname , Mutant * mutant) {
   FILE * fmutant ;
   char buf[1024] , type[32] ;
   int var ;

   //SHOWVAR(file) ;

   fmutant = fopen(file , "r") ;
   if (fmutant == NULL) {
      // TODO: raise exception
   }

   /*
    * try to find line:
    * c mutant: 1 (all positive|negated|negative)
    */
   for (; fgets(buf , 1024 , fmutant) ;) {
      if (sscanf(buf , "c mutant: %d (all %[a-z]s)" , &var , type) == 2) {
         break ;
      }
   }

   //SHOWVAR(var) ;
   //SHOWVAR(type) ;

   /*
    * The following code only works for valid inputs
    */
   if (type[0] == 'p') {
      // positive
      new (mutant) Mutant(POSITIVE  , var , fname) ;
   } else if (type[5] == 'e') {
      // negated
      new (mutant) Mutant(TOGGLE    , var , fname) ;
   } else {
      // negative
      new (mutant) Mutant(NEGATIVE  , var , fname) ;
   }

   fclose(fmutant) ;
   return ;
}

static char * getFileName(const char * head ,const char * tail) {
   /*
    * find the position after last '/'
    * xxx/xxx/mutant_file
    *         ^ here
    */
   for (; tail > head && *(tail - 1) != '/' ; -- tail) ;
   return (char *)tail ;
}

