#include <set>
#include "sat.h"
#include "clause.h"
#include "debug.h"

SatAgent::SatAgent() {
    _solver = new SOLVER();
#ifdef DEBUG
    _solver->verbosity = 1 ;
//    _solver->remove_satisfied = false ;
#endif
}

SatAgent::~SatAgent() {
    delete _solver;
}

VARIABLE SatAgent::newVariable() {
#ifdef MINISAT
    return _solver->newVar();
#endif
}

void SatAgent::assumeVariable(VARIABLE& var, bool value) {
#ifdef MINISAT
    _assumptions.push(mkLit(var, value));
#endif
}

void SatAgent::addClauses(Clauses& clause) {
   for(std::set<int>::iterator it=clause.variables.begin();
       it != clause.variables.end(); it++)
      if(_map.find(*it) == _map.end())
         _map[*it] = newVariable();
   for(int i = 0; i < clause.clauses.size(); ++i) {
      SatClause sat_clause;
      for(int j = 0; j < clause.clauses[i].size(); ++j)
         sat_clause.addLiteral(_map[clause.clauses[i][j]], clause.sign[i][j]);
      addClause(sat_clause);
   }
}

void SatAgent::addClause(SatClause& clause) {
#ifdef MINISAT
    _solver->addClause(clause.getClause());
#endif
}

SatReason SatAgent::getReason() {
#ifdef MINISAT
   return (SatReason)_solver->getReason();
#endif
}

Assignment SatAgent::getAssignment() {
   Assignment result;
   for(std::map<int, VARIABLE>::iterator it = _map.begin();
       it != _map.end(); ++it) {
//      dprintf("value(%3d) = %d\n" , it->first , getValue(it->first)) ;
      result._map[it->first] = getValue(it->first) ;
   }
   //dprintf("\n") ;
   return result;
}

#ifdef MINISAT
bool SatAgent::solve(LITERAL lit) {
   return _solver->solve(lit) ;
}
#endif

bool SatAgent::solve() {
#ifdef MINISAT
    return _solver->solve(_assumptions);
#endif
}

bool SatAgent::getValue(int var) {
#ifdef MINISAT
   assert(_solver->modelValue(_map[var]) != l_Undef) ;
   return _solver->modelValue(_map[var]) == l_True;
#endif
}

void SatClause::addLiteral(VARIABLE& var, bool sign) {
#ifdef MINISAT
   /*
    * for mini sat,  sign == 0 =>  var
    *                sign == 1 => !var
    */
   _clause.PUSH(mkLit(var, !sign));
#endif
}

