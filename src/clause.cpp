#include <map>
#include "clause.h"
#include "global.h"
#include "debug.h"

bool Assignment::getValue(int var) const {
   return _map.find(var)->second;
}

void Assignment::setValue(int var,bool x) {
	_map[var] = x;
}

void Assignment::show(FILE * out) const {
   for (std::map<int , bool>::const_iterator it = _map.begin() ;
         it != _map.end() ; ++ it) {
      if (out)
         fprintf(out , "%d => %d\n" , it->first , it->second) ;
      else
         dprintf("%d => %d\n" , it->first , it->second) ;
   }
   return ;
}

ostream& operator<<(ostream& stream, const Assignment& asn) {
	stream << "SAT" << endl;
   for (std::map<int , bool>::const_iterator it = asn._map.begin() ;
         it != asn._map.end() ; ++ it) {
		stream << (it->second?1:-1)*it->first << " ";
	}
	stream << "0" << endl;
	return stream;
}

Clauses::Clauses() {

}

bool Clauses::isValid(int literal) {
   return variables.find(literal) != variables.end();
}

Clauses& Clauses::operator+=(const Clauses &b) {
	for(vector<vector<int> >::const_iterator it = b.clauses.begin();
			it!=b.clauses.end(); it++)
		clauses.push_back(*it);
	for(vector<vector<bool> >::const_iterator it = b.sign.begin();
			it!=b.sign.end(); it++)
		sign.push_back(*it);
	for(set<int>::const_iterator it = b.variables.begin();
			it!=b.variables.end(); it++)
		variables.insert(*it);
	return *this;
}

const Clauses Clauses::operator+(const Clauses &b) const {
	return (Clauses)*this += b;
}

bool Clauses::verify(const Assignment &a) const {
	int i,j;
	for(i=0; i<(int)clauses.size(); i++) {
		const vector<int> &cls = clauses[i];
		const vector<bool> &sg = sign[i];
		for(j=0; j<(int)cls.size(); j++)
			if(a.getValue(cls[j])^(!sg[j])) break;
		if(j==(int)cls.size()) return 0;
	}
	return 1;
}

ostream& operator<<(ostream& ostr, const Clauses &b) {
	int i,j,vmax = *(b.variables.rbegin());
	ostr << "p cnf " << vmax << " " << b.clauses.size() << endl;
	for(i=0; i<b.clauses.size(); i++) {
		const vector<int> &var = b.clauses[i];
		const vector<bool> &sign = b.sign[i];
		for(j=0; j<var.size(); j++)
			ostr << (sign[j]?1:-1)*var[j] << " ";
		ostr << "0\n";
	}
	return ostr;
}

int Property::getSign(int var) const {
   int rv = 0 ;

   for (int csize = clauses.size() , i = 0 ; i < csize ; ++ i) {
      for (int size = clauses[i].size() , j = 0 ; j < size ; ++ j) {
         if (clauses[i][j] == var) {
            int s = sign[i][j] ? 1 : -1 ;
            if (rv == 0) rv = s ;
            else if (rv != s) return 0;
         }
      }
   }
   return rv ;
}

Mutant::Mutant(MutantType t, int l , string n) : name(n) {
   type = t;
   literal = l;
   alive = UNKNOWN;
   for(int i = 0; i < property_num; ++i)
      property_list.push_back(&properties[i]);
}

Model Model::getModel(Mutant& mut) {
   Model result;
   for(int i = 0; i < clauses.size(); ++i) {
      vector<bool> new_sign;
      for(int j = 0; j < clauses[i].size(); ++j)
         if(clauses[i][j] == mut.getVariable())
            switch(mut.getType()) {
               case POSITIVE:
                  new_sign.push_back(true);
                  break;
               case NEGATIVE:
                  new_sign.push_back(false);
                  break;
               case TOGGLE:
                  new_sign.push_back(!sign[i][j]);
                  break;
            }
         else
            new_sign.push_back(sign[i][j]);
      result.sign.push_back(new_sign);
   }
   result.clauses = clauses;
   result.variables = variables;
   return result;
}

