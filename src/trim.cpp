#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <utility>
#include <map>
#include <set>
#include "trim.h"
#include "debug.h"

using namespace std;

inline static int _sg(int x) { return x?(x>0?1:-1):0; }
inline static int _abs(int x) { return x>=0?x:0-x; }
inline static int _tf2sg(bool x) { return x?1:-1; }
inline static bool _sg2tf(int x) { return x>0?1:0; }

/* for disjoint set */
class DNode {
	friend bool dmerge(DNode*,DNode*,bool);
	public:
	   int id;
		void init(int iid) {
			id=iid;
			rank=0;
			r=this;
			x=0;
		}
		bool getx() {
			rep();
			return x;
		}
		DNode* rep() {
			if(r!=this) {
				r->rep();
				x^=r->x;
				r=r->r;
			}
			return r;
		}
	private:
		int rank;
		DNode *r;
		bool x;
};

/* union two nodes, return 0 iff conflict occurs */
bool dmerge(DNode *_v,DNode *_u,bool x) {
	DNode *v = _v->rep();
	DNode *u = _u->rep();
	x^=_v->x^_u->x;
	if(v==u) return x==0;
	if(v->rank<u->rank) {
		v->r=u;
		v->x=x;
	} else {
		u->r=v;
		u->x=x;
		if(v->rank==u->rank) v->rank++;
	}
	return 1;
}

Assignment trim_remap(const Assignment &reduced, map<int,int>rmap) {
	/* note: assume input assignment-map pair is indeed valid */
	int v,u;
	Assignment full;
	for(map<int,int>::iterator it = rmap.begin(); it != rmap.end(); it++) {
		v = it->first;
		u = it->second;
		if(u == _trim_true) full.setValue(v,true);
		else if(u == _trim_false) full.setValue(v,false);
		else if(u == v) full.setValue(v,reduced.getValue(v));
		else full.setValue(v,(u<0?1:0)^reduced.getValue(_abs(u)));
	}
	return full;
}

BiClause::BiClause() {}
BiClause::BiClause(int vi,int ui):v(vi),u(ui) {
	if(_abs(v)>_abs(u)) std::swap(v,u);
}

BiClause BiClause::rev() const {
	return BiClause(-u,-v);
}

bool BiClause::relsign() const {
	return _sg(u)==_sg(v)?1:0;
}

const bool BiClause::operator<(const BiClause &b) const {
	if(v!=b.v) return v<b.v;
	if(u!=b.u) return u<b.u;
	return 0;
}

void TrimAgent::initialize(const Clauses& clss) {
	int i,v;
	vmax=0;
	rmap.clear();
	// initialize variable map to oneself
	for(set<int>::iterator it=clss.variables.begin();
			it!=clss.variables.end(); it++) {
		v = *it;
		if(v>vmax) vmax=v;
		rmap[v] = v;
	}
	// initialize disjoint set
	vset = new DNode[vmax+1];
	for(i=0; i<=vmax; i++)
		vset[i].init(i);
}

void TrimAgent::finalize() {
	delete vset;
}

int TrimAgent::getSymbol(int v) {
	if(rmap[v]==_trim_false || rmap[v]==_trim_true) {
		puts("[trim] const var not eliminated, should not occur!");
		return 0;
	}
	if(vset[_abs(v)].rep()==vset+_abs(v)) return v;
	return (vset[_abs(v)].rep())->id*(vset[_abs(v)].getx()?-1:1)*_sg(v);
}

bool TrimAgent::elimConstVar(Clauses& clss) {
	int i,j,v;
	bool altflag = 0;
	failflag = 0;
	/* search for unit clause to determine ConstVar */
	for(i=0; i<clss.clauses.size(); i++) {
		vector<int> &var = clss.clauses[i];
		vector<bool> &sign = clss.sign[i];
		if(var.size()!=1) continue;
		v = var[0] * _tf2sg(sign[0]);
		// if conflicting return 0
		if(v>0 && rmap[_abs(v)]==_trim_false
				|| v<0 && rmap[_abs(v)]==_trim_true) {
			failflag = 1;
			return 1;
		}
		// else set v as const variable
		rmap[abs(v)] = v>0? _trim_true : _trim_false;
		altflag=1;
	}
	/* eliminate const variables */
	for(i=0; i<clss.clauses.size(); i++) {
		vector<int> &var = clss.clauses[i];
		vector<bool> &sign = clss.sign[i];
		for(j=0; j<var.size(); j++) {
			v = var[j] * _tf2sg(sign[j]);
			if(rmap[abs(v)]!=_trim_true && rmap[abs(v)]!=_trim_false) continue;
			int sg = _sg(v)*(rmap[abs(v)]==_trim_true?1:-1);
			if(sg>0) { // eliminate clause
				altflag = 1;
				var = clss.clauses.back();
				sign = clss.sign.back();
				clss.clauses.pop_back();
				clss.sign.pop_back();
				i--;
				break;
			} else if(sg<0) { // eliminate variable
				altflag = 1;
				var[j] = var.back();
				sign[j] = sign.back();
				var.pop_back();
				sign.pop_back();
				j--;
				continue;
			}
		}
		if(var.size()==0) {
			failflag = 1;
			return 1;
		}
	}
	return altflag;
}

bool TrimAgent::elimCorrelated(Clauses& clss) {
	int i,j,v,u;
	bool altflag = 0;
   set<BiClause> bcset;
	failflag = 0;
	/* search for Biclauses and deduce correlation if possible */
	for(i=0; i<clss.clauses.size(); i++) {
		vector<int> &var = clss.clauses[i];
		vector<bool> &sign = clss.sign[i];
		if(var.size()!=2) continue;
		v = var[0] * _tf2sg(sign[0]);
		u = var[1] * _tf2sg(sign[1]);
		bcset.insert(BiClause(v,u));
		if(bcset.find(BiClause(-v,-u)) != bcset.end()) { // counterpart found
			//printf("[trim] relation pair <%d %d>\n",v,u);
			if(!dmerge(vset+_abs(v),vset+_abs(u),BiClause(v,u).relsign())) {
				//printf("<%d %d>\n",v,u);
				//printf("[%d %d: %d %d]\n",vset[_abs(v)].rep()->id,vset[_abs(u)].rep()->id,
				//		vset[_abs(v)].getx(),vset[_abs(u)].getx());
				// conflict occurs
				failflag = 1;
				return 1;
			}
		}
	}
	/* update clauses and replace variables with their correspondant */
   for(i=0; i<clss.clauses.size(); i++) {
		vector<int> &var = clss.clauses[i];
		vector<bool> &sign = clss.sign[i];
		for(j=0; j<var.size(); j++) {
			v = var[j] * _tf2sg(sign[j]);
			u = getSymbol(v);
			//if(v!=u) printf("[trim] %d replaced with %d\n",v,u);
			var[j] = _abs(u);
			sign[j] = _sg2tf(u);
			if(u!=v) altflag = 1;
		}
	}
	/* trim down trivial variable/clauses */
	for(i=0; i<clss.clauses.size(); i++) {
		vector<int> &var = clss.clauses[i];
		vector<bool> &sign = clss.sign[i];
		set<int> varset;
		for(j=0; j<var.size(); j++) {
			v = var[j] * _tf2sg(sign[j]);
			if(varset.find(v)!=varset.end()) { // repeating variable
				altflag = 1;
				var[j] = var.back();
				sign[j] = sign.back();
				var.pop_back();
				sign.pop_back();
				j--;
				continue;
			}
			if(varset.find(-v)!=varset.end()) { // trivially true
				altflag = 1;
				var = clss.clauses.back();
				sign = clss.sign.back();
				clss.clauses.pop_back();
				clss.sign.pop_back();
				i--;
				break;
			}
			varset.insert(v);
		}
	}
	return altflag;
}

void TrimAgent::updateVariablesSet(Clauses& clss) {
	int v,p;
	for(set<int>::iterator it = clss.variables.begin();
			it != clss.variables.end(); ) {
		v = *it;
		p = vset[v].rep()->id;
		if(rmap[p]==_trim_true || rmap[p]==_trim_false) {
			if(vset[v].getx()) rmap[v] = _trim_true - rmap[p] + _trim_false;
			else rmap[v] = rmap[p];
			it++;
			continue;
		}
		rmap[v] = getSymbol(v);
		if(rmap[v]==v) {
			it++;
			continue;
		}
		clss.variables.erase(it++);
	}
}

pair<Clauses, map<int,int> >
TrimAgent::trim(Clauses inputClauses, bool &satFlag)
{
	bool alt;
	initialize(inputClauses);
	satFlag = 1;
	do {
		dprintf("trim iteration\n");
		alt = elimConstVar(inputClauses);
		if(failflag) {
			satFlag = 0;
			break;
		}
		alt |= elimCorrelated(inputClauses);
		if(failflag) {
			satFlag = 0;
			break;
		}
	} while(alt);
	updateVariablesSet(inputClauses);
	finalize();
	return pair<Clauses,map<int,int> >(inputClauses, rmap);
}

pair<Clauses, map<int,int> >
TrimAgent::trim(Clauses inputClauses) {
	bool negFlag;
	return trim(inputClauses, negFlag);
}
