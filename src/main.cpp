#include <iostream>
#include <list>
#include <time.h>
#include "clause.h"
#include "parser.h"
#include "procedure.h"
#include "debug.h"
#include "config.h"

int mutant_num, property_num;
Model model;
Mutant *mutants;
Property *properties;
std::list<Mutant*> mutant_list;
time_t start_time , pre_time;

static void cleanUp() {
   delete[] mutants;
   delete[] properties;
}

int main(int argc, char **argv) {
   pre_time = start_time = time(NULL);
   mutants = NULL ;
   properties = NULL ;

   parseProperty(argv[2]);
   parseMutant(argv[1]);

   generateModel(argv[1]);

   // Global variables are set now
#ifdef FIRST
   printStat();
   firstPass();
#endif

#ifdef SECOND
   printStat();
   secondPass();
#endif

#ifdef THIRD
   printStat();
   thirdPass();
#endif

#ifdef LAST
   printStat();
   lastPass() ;
#endif

   printStat();
   printOutput(argv[3]);

   cleanUp();
   return 0;
}

